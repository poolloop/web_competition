#### 项目介绍
企业级项目实训

#### 软件架构
基于ssm，Shopping(管理后台)、pc(电脑端)以及mobile(手机端)多项目使用http对象进行json数据通信的电商项目

#### 安装教程
一、使用git bash命令行工具将项目克隆到本地：
1.  安装git bash命令行工具,传送门：https://git-scm.com/downloads；
2.  安装完成后，在本地新建一个文件夹web_competition，点击进去，鼠标右击选择Git Bash Here；
3.  如果之前没有进行用户名和密码的配置，则首先输入git config user.email "用户名",再次输入user.password "密码"；
4.  配置完毕后，输入git init即初始化本地仓库， 这时，本地文件夹会生成一个.git的隐藏文件夹；
5.  再次输入git clone https://gitee.com:443/inexorableimp/web_competition.git,将项目克隆到本地；
二、将数据库文件导入到本地数据库
1.  mysql安装教程略，使用数据库连接工具navicat连接本地数据库，创建一个连接，点击进去创建数据库shopping
2.  导入数据库文件(数据库文件位于Shopping项目下sql文件夹内)，如图：
![输入图片说明](https://images.gitee.com/uploads/images/2019/1223/144909_2c0c7b4f_2165753.jpeg "create database and tables.jpg")
三、运行项目
1.  使用Eclipse Oxygen IDE 进行项目导入，file===>import-Projects from Folder or Archive,点击finish完成项目导入
2. 如果是本地运行，本机tomcat端口请设为8080，注意pc及mobile的数据都是通过Shopping项目查询并转发的，所以得先启Shopping项目
右击选择Run as===>Run On Server,浏览器输入http://localhost:8080/Shopping/login,进行后台登录，如图：
![输入图片说明](https://images.gitee.com/uploads/images/2019/1223/150043_3cb7b41d_2165753.png ")H7]$1IMH_[OZ7GMM[{QE9S.png")
输入admin/password登入成功后即可看到后台首页,如图：
![输入图片说明](https://images.gitee.com/uploads/images/2019/1223/150250_50aa76dd_2165753.png "B934O8LTW4P52C7LH_BV[KL.png")

#### 使用说明
1.  创建一个自定义接口
2.  使用mybatis generator生成mapper.xml等文件




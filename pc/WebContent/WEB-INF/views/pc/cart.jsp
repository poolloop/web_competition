<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<spring:url value="/resources/pc/assets/css" var="css" />
<spring:url value="/resources/pc/assets/js" var="js" />
<c:set var="root" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html>
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<title>SHOP</title>

<!-- mobile settings -->
<meta name="viewport"
	content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

<!-- CORE CSS -->
<link href="${css}/bootstrap.min.css" rel="stylesheet">

<!-- THEME CSS -->
<link href="${css}/essentials.css" rel="stylesheet" type="text/css" />
<link href="${css}/layout.css" rel="stylesheet" type="text/css" />
<link href="${root}/resources/pc/assets/js/bootstrap-touchspin-master/src/jquery.bootstrap-touchspin.js" rel="stylesheet" type="text/css" />
<link href="${root}/resources/pc/assets/js/toastr/build/toastr.min.css" rel="stylesheet" />
<!-- PAGE LEVEL SCRIPTS -->
<link href="${css}/header-1.css" rel="stylesheet" type="text/css" /><link href="${css}/layout-shop.css" rel="stylesheet" type="text/css" />

<link href="${css}/color_scheme/orange.css" rel="stylesheet" type="text/css" id="color_scheme" />

<jsp:include page="include/top.jsp"></jsp:include>
</head>
<body class="smoothscroll enable-animation">
	<!-- wrapper -->
	<div id="wrapper">
		<jsp:include page="include/head.jsp"></jsp:include>
		<section class="page-header page-header-xs">
			<div class="container">
				<h1>我的购物车</h1>
				<ol class="breadcrumb">
					<li><a href="${root}/pc/index">首页</a></li>
					<li>购物车</li>
				</ol>
			</div>
		</section>
		<!-- /PAGE HEADER -->
		<!-- 购物车 -->
		<section>
			<div class="container">
				<!-- 如果购物车为空 -->
				<div id="msgDiv" class="panel panel-default" style="display: none">
					<div class="panel-body">
						<strong style='font-size: 25px'>购物车里无商品!</strong><br />
						随便看看，优惠多多，赶紧抢购吧！
					</div>
				</div>
				<!-- 如果购物车为空 -->
				<!-- CART -->
				<div class="row">
					<!-- LEFT -->
					<div class="col-lg-9 col-sm-8">
						<!-- CART -->
						<form class="cartContent clearfix" method="post" action="#">
							<!-- cart content -->
							<div id="cartContent">
								
								<button id="updateCart" type="button" class="btn btn-success margin-top-20 pull-right noradius">
										 更新购物车
									</button>
									<button id="removeCart" type="button" class="btn btn-danger margin-top-20 pull-right noradius">
										清空购物车
									</button>
								<div class="clearfix"></div>
							</div>
							<!-- /cart content -->

						</form>
						<!-- /CART -->
					</div>
					<!-- RIGHT -->
					<div class="col-lg-3 col-sm-4">
						<button id="payCart" type="button" class="btn btn-success margin-top-20 pull-right noradius">立即支付</button>
					</div>
				</div>
				<!-- /CART -->
			</div>
		</section>
		<!-- /购物车 -->
		<jsp:include page="include/footer.jsp"></jsp:include>
	</div>
	<!-- /wrapper -->
	<!-- JAVASCRIPT FILES -->
	<script src="${root}/resources/pc/assets/js/bootstrap-touchspin-master/src/jquery.bootstrap-touchspin.js"></script>
	<script src="${root}/resources/pc/assets/js/toastr/build/toastr.min.js"></script>
	<script src="${root}/resources/pc/assets/js/swiper.min.js"></script>

</body>
<script type="text/javascript">
	$(function() {
		$("#payCart").bind('click', function() {
			window.location.href="${pageContext.request.contextPath}/pc/payment?${_csrf.parameterName}=${_csrf.token}";
		});
	});
	
</script>
</html>
package com.sybinal.shop.controller.pc;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.sybinal.shop.common.ApiResponseObject;
import com.sybinal.shop.common.ApiServiceException;
import com.sybinal.shop.model.Contect;
import com.sybinal.shop.service.api.contect.ApiContectService;

@Controller
public class PCContectController {

	@Autowired
	ApiContectService contectService;

	@RequestMapping(value = "pc/account/contact/list")
	public ModelAndView contactList(HttpServletRequest req) throws ApiServiceException {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("pc/account/contact_list");
		return modelAndView;
	}

	@RequestMapping(value = "pc/contect/add", method = RequestMethod.POST)
	@ResponseBody
	public ApiResponseObject addContect(Contect contect, HttpServletRequest req) throws ApiServiceException {
		return null;
	}

	@RequestMapping(value = "pc/contect/remove", method = RequestMethod.POST)
	@ResponseBody
	public ApiResponseObject removeContect(@RequestBody Contect contect, HttpServletRequest req)
			throws ApiServiceException {
		return null;
	}

	@RequestMapping(value = "pc/contect/default", method = RequestMethod.POST)
	@ResponseBody
	public ApiResponseObject defaultContect(@RequestBody Contect contect, HttpServletRequest req)
			throws ApiServiceException {
		return null;
	}
}
